import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gyapuapp/profile.dart';
import 'package:gyapuapp/register.dart';
import 'package:share_plus/share_plus.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

Future<UserCredential> signInWithFacebook() async {
  // Trigger the sign-in flow
  final LoginResult loginResult = await FacebookAuth.instance.login();

  // Create a credential from the access token
  final OAuthCredential facebookAuthCredential =
      FacebookAuthProvider.credential(loginResult.accessToken!.token);

  // Once signed in, return the UserCredential
  return FirebaseAuth.instance.signInWithCredential(facebookAuthCredential);
}

GoogleSignInAccount? _userObj;

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            // crossAxisAlignment: CrossAxisAlignment.,
            children: [
              SizedBox(
                height: 30,
              ),
              Center(
                child: SizedBox(
                  height: 90,
                  width: 130,
                  child: Image(
                    image: AssetImage('lib/assets/gyapu1.png'),
                  ),
                ),
              ),
              SizedBox(height: 50),
              Text(
                'Login',
                style: TextStyle(
                    fontSize: 25,
                    color: Colors.amber[800],
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width - 80,
                child: Form(
                  child: Column(
                    // mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 40),
                      Text(
                        'Email',
                        style: TextStyle(fontSize: 18, color: Colors.grey[700]),
                      ),
                      SizedBox(height: 20),
                      Container(
                        height: 45,
                        child: TextFormField(
                          decoration: InputDecoration(
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 1),
                              borderRadius: BorderRadius.circular(5),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(width: 1, color: Colors.amber),
                              borderRadius: BorderRadius.circular(5),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 40),
                      Text(
                        'Password',
                        style: TextStyle(fontSize: 18, color: Colors.grey[700]),
                      ),
                      SizedBox(height: 20),
                      Container(
                        height: 45,
                        child: TextFormField(
                          obscureText: true,
                          decoration: InputDecoration(
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 1),
                              borderRadius: BorderRadius.circular(5),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(width: 1, color: Colors.amber),
                              borderRadius: BorderRadius.circular(5),
                            ),
                            suffixIcon: Icon(Icons.remove_red_eye),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      InkWell(
                        child: Text('Forget Password'),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 40,
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width - 80,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.amber[800],
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Profile(
                            photo: 'lib/assets/rock.jpg',
                            name: 'Dwayne',
                            mode: 0),
                      ),
                    );
                  },
                  child: Text('Login'),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text('or login with'),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      // Share.share('sdjf');
                    },
                    child: InkWell(
                      child: CircleAvatar(
                        radius: 22,
                        child: Icon(
                          FontAwesomeIcons.facebookF,
                          color: Colors.white,
                        ),
                        backgroundColor: Color(0xff39579A),
                      ),
                      onTap: () async {
                        signInWithFacebook();
                      },
                    ),
                  ),
                  SizedBox(
                    width: 30,
                  ),
                  InkWell(
                    child: CircleAvatar(
                      radius: 22,
                      child: Icon(
                        FontAwesomeIcons.google,
                        color: Colors.white,
                      ),
                      backgroundColor: Color(0xffDF4A32),
                    ),
                    onTap: () async {
                      final GoogleSignInAccount? googleUser =
                          await GoogleSignIn().signIn().then((userData) {
                        _userObj = userData!;
                      });
                      if (_userObj != null) {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (Context) => Profile(
                                name: _userObj!.displayName!,
                                photo: _userObj!.photoUrl!,
                                mode: 1),
                          ),
                        );
                      }
                      final GoogleSignInAuthentication? googleAuth =
                          await googleUser?.authentication;

                      final credential = GoogleAuthProvider.credential(
                        accessToken: googleAuth?.accessToken,
                        idToken: googleAuth?.idToken,
                      );
                      // print(credential);
                      print(_userObj);
                    },
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              InkWell(
                child: Text(
                  "Don't have an account? Register",
                  style: TextStyle(
                      color: Colors.black54, fontWeight: FontWeight.bold),
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => const Register(),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
