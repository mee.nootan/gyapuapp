import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:gyapuapp/aboutUs.dart';
import 'package:share_plus/share_plus.dart';

class Profile extends StatefulWidget {
  final String photo, name;
  final int mode;
  const Profile(
      {Key? key, required this.photo, required this.name, required this.mode})
      : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Material(
        child: CustomScrollView(
          slivers: [
            SliverPersistentHeader(
              delegate: MySliverAppBar(
                  expandedHeight: 200,
                  photo: widget.photo,
                  name: widget.name,
                  mode: widget.mode),
              pinned: true,
            ),
            // SliverList(
            //   delegate: SliverChildBuilderDelegate(
            //     (_, index) => ListTile(
            //       title: Text("Index: $index"),
            //     ),
            //   ),
            // )
            SliverFillRemaining(
              child: Column(
                children: [
                  SizedBox(height: 60),
                  customListTile(context, Icons.group_work_outlined,
                      "Refer and Earn", "971EDB5", Icons.share, 1),
                  customListTile(context, Icons.add_box_outlined, "My Orders",
                      "", Icons.navigate_next, 0),
                  customListTile(context, Icons.grading, "My Review", "",
                      Icons.navigate_next, 0),
                  customListTile(context, Icons.contact_page_outlined,
                      "Contact Us", "", Icons.navigate_next, 0),
                  customListTile(context, Icons.share, "Share Our App", "",
                      Icons.navigate_next, 1),
                  customListTile(context, Icons.note_add_outlined,
                      "Terms and Conditions", "", Icons.navigate_next, 0),
                  customListTile(context, Icons.note_add_outlined,
                      "Terms and Conditions", "", Icons.navigate_next, 0),
                  customListTile(context, Icons.privacy_tip_outlined,
                      "Privacy Policy", "", Icons.navigate_next, 0),
                  customListTile(context, Icons.info, "About Us", "",
                      Icons.navigate_next, 2),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget customListTile(BuildContext context, IconData leadingIcon, String title,
    String subText, IconData trailingIcon, int sh) {
  void share() {
    Share.share('asdf');
  }

  void def() {}
  return Column(
    children: [
      ListTile(
        leading: Icon(leadingIcon, size: 25),
        title: Row(
          children: [
            Text(
              title,
              style: TextStyle(
                  fontWeight: FontWeight.w400, color: Colors.grey[700]),
            ),
            SizedBox(width: 20),
            Text(
              subText,
              style: TextStyle(color: Colors.grey),
            ),
          ],
        ),
        trailing: Icon(trailingIcon),
        onTap: () {
          if (sh == 1) {
            share();
          }
          if (sh == 2) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => About(),
              ),
            );
          }
        },
      ),
      // SizedBox(height: 2),
    ],
  );
}

Future<void> _signOut() async {
  await FirebaseAuth.instance.signOut();

  print('signout');
  if (FirebaseAuth.instance.currentUser == null) {
    print('fj');
  }
  ;
}

class MySliverAppBar extends SliverPersistentHeaderDelegate {
  final double? expandedHeight;
  final String name, photo;
  final int mode;

  MySliverAppBar(
      {@required this.expandedHeight,
      required this.name,
      required this.photo,
      required this.mode});
  void share() {
    // Share.share('asd');
    print('object');
    Share.share('sdjf');
  }

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Stack(
      clipBehavior: Clip.none,
      fit: StackFit.expand,
      children: [
        Container(
          color: Colors.white,
        ),
        Center(
          child: SingleChildScrollView(
            child: Opacity(
              opacity: (1 - shrinkOffset / expandedHeight!),
              child: Column(
                children: [
                  Stack(
                    children: [
                      Container(
                        child: Image(
                          image: AssetImage('lib/assets/amberbk.jpg'),
                        ),
                      ),
                      Positioned(
                        right: 10,
                        child: IconButton(
                          onPressed: () {},
                          icon: Icon(Icons.settings, color: Colors.black54),
                        ),
                      ),
                      Positioned(
                        top: 10,
                        left: MediaQuery.of(context).size.width / 2 - 40,
                        child: (mode == 1)
                            ? CircleAvatar(
                                backgroundColor: Colors.transparent,
                                radius: 40,
                                backgroundImage: NetworkImage(photo),
                              )
                            : CircleAvatar(
                                backgroundColor: Colors.transparent,
                                radius: 40,
                                backgroundImage: AssetImage(photo),
                              ),
                      ),
                      Positioned(
                        top: 100,
                        left: MediaQuery.of(context).size.width / 2 - 30,
                        right: MediaQuery.of(context).size.width / 3 - 20,
                        child: Text(
                          name,
                          style: TextStyle(fontSize: 20, color: Colors.white),
                        ),
                      ),
                      SizedBox(height: 20),
                      IconButton(
                        icon: Icon(Icons.arrow_back, color: Colors.black54),
                        onPressed: () async {
                          _signOut();
                          // print(credential);
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
        // Container(
        //   height: 50,
        //   width: 200,
        //   color: Colors.pink,
        // ),
        Opacity(
          opacity: shrinkOffset / expandedHeight!,
          child: Row(
            children: [
              SizedBox(
                width: 20,
              ),
              Text(
                name,
                style: TextStyle(
                  color: Colors.amber[800],
                  fontWeight: FontWeight.w500,
                  fontSize: 20,
                ),
              ),
            ],
          ),
        ),
        Positioned(
          top: (expandedHeight! + 100) / 2 - shrinkOffset,
          left: 5,
          right: 5,
          child: Opacity(
            opacity: (1 - shrinkOffset / expandedHeight!),
            child: Card(
              elevation: 3,
              child: SizedBox(
                height: expandedHeight! - 100,
                width: MediaQuery.of(context).size.width - 50,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  // crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('0'),
                        Text('My Wishlists'),
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('100'),
                        Text('G-Cash'),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  double get maxExtent => expandedHeight!;

  @override
  double get minExtent => kToolbarHeight;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;
}
