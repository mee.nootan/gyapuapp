import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

bool chk1 = false;

class _RegisterState extends State<Register> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.grey,
        ),
        backgroundColor: Colors.grey[50],
        elevation: 0.3,
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            // crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Center(
                child: SizedBox(
                  width: 95,
                  height: 100,
                  child: Image(
                    image: AssetImage('lib/assets/gyapu1.png'),
                  ),
                ),
              ),
              Card(
                elevation: 3,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: Container(
                    child: Form(
                      child: Column(
                        children: [
                          Text(
                            'Sign Up',
                            style: TextStyle(
                                fontSize: 25,
                                color: Colors.amber[800],
                                fontWeight: FontWeight.bold),
                          ),
                          customFormField('Name'),
                          customFormField('Email'),
                          customFormFieldPhone('Mobile Number', context),
                          customFormPasswordField('Password'),
                          customFormPasswordField('Confirm Password'),
                          customFormField('Referral Code(Optional)'),
                          SizedBox(height: 20),
                          Row(
                            children: [
                              Checkbox(
                                  value: chk1,
                                  onChanged: (val) {
                                    setState(() {
                                      chk1 = !val!;
                                      print(chk1);
                                    });
                                  }),
                              Container(
                                width: MediaQuery.of(context).size.width - 100,
                                child: Text(
                                    "I've Read And Understood Gyapu's Terms and Conditions"),
                              ),
                            ],
                          ),
                          SizedBox(height: 20),
                          Row(
                            children: [
                              Checkbox(
                                  value: chk1,
                                  onChanged: (val) {
                                    setState(() {
                                      chk1 = !val!;
                                      print(chk1);
                                    });
                                  }),
                              Text("I'm not a robot"),
                            ],
                          ),
                          SizedBox(height: 20),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              customButton('Cancel', Colors.red),
                              SizedBox(
                                width: 20,
                              ),
                              customButton('Register', Colors.grey),
                            ],
                          ),
                          SizedBox(height: 20),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

Widget customFormField(String label) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      SizedBox(height: 40),
      Text(
        label,
        style: TextStyle(fontSize: 18, color: Colors.grey[700]),
      ),
      SizedBox(height: 10),
      Container(
        height: 45,
        child: TextFormField(
          decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(width: 1),
              borderRadius: BorderRadius.circular(5),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(width: 1, color: Colors.amber),
              borderRadius: BorderRadius.circular(5),
            ),
          ),
        ),
      ),
    ],
  );
}

Widget customFormFieldPhone(String label, BuildContext context) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      SizedBox(height: 40),
      Text(
        label,
        style: TextStyle(fontSize: 18, color: Colors.grey[700]),
      ),
      SizedBox(height: 10),
      Container(
        height: 45,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: Center(
                  child: Text(
                "+977",
                style: TextStyle(fontSize: 17),
              )),
              width: 40,
              height: 45,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(5),
                  bottomLeft: Radius.circular(5),
                ),
                color: Colors.grey[300],
              ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width - 95,
              child: TextFormField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: 1),
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(5),
                      bottomRight: Radius.circular(5),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: 1, color: Colors.amber),
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(5),
                      bottomRight: Radius.circular(5),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    ],
  );
}

Widget customFormPasswordField(String label) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      SizedBox(height: 40),
      Text(
        label,
        style: TextStyle(fontSize: 18, color: Colors.grey[700]),
      ),
      SizedBox(height: 10),
      Container(
        height: 45,
        child: TextFormField(
          obscureText: true,
          decoration: InputDecoration(
            suffixIcon: Icon(
              Icons.remove_red_eye,
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(width: 1),
              borderRadius: BorderRadius.circular(5),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(width: 1, color: Colors.amber),
              borderRadius: BorderRadius.circular(5),
            ),
          ),
        ),
      ),
    ],
  );
}

Widget customButton(String label, Color btColor) {
  return ElevatedButton(
    onPressed: () {},
    child: Text(
      label,
      style: TextStyle(fontSize: 17),
    ),
    style: ElevatedButton.styleFrom(
      primary: btColor,
    ),
  );
}
