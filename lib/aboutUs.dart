import 'package:flutter/material.dart';

class About extends StatefulWidget {
  const About({Key? key}) : super(key: key);

  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.grey,
        ),
        backgroundColor: Colors.white,
        title: Text(
          "About Us",
          style: TextStyle(
            color: Colors.amber[800],
          ),
        ),
      ),
      body: Container(
          child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 10.0),
            Text(
              "About Us",
              style: TextStyle(fontSize: 25),
            ),
            SizedBox(height: 10.0),
            Text(
              "GYAPU is the most rewarding online shopping platform led by Nepali Entrepreneurs with the objective of expanding and flourishing e-commerce industries in Nepal and across South East Asia. The primary objective of GYAPU is to build the Nepali Global Brand and promote domestic Nepali products in Nepal and throughout the globe.\n\n",
              style: TextStyle(fontSize: 15),
            ),
            Text(
              "GYAPU pledges to disintermediate the monopoly of mediators and existing e-commerce industrial trends. GYAPU envisions to uplifting the local business enthusiasts and overall Economy of Nepal while simultaneously creating job opportunities nationwide",
              style: TextStyle(fontSize: 15),
            ),
          ],
        ),
      )),
    );
  }
}
