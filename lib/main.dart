import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:gyapuapp/home.dart';
import 'package:gyapuapp/login.dart';
import 'package:gyapuapp/profile.dart';
import 'package:gyapuapp/register.dart';
import 'package:flutter/cupertino.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(
    MaterialApp(
      home: MyApp(),
    ),
  );
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int selectedIndex = 0;

  static const List<Widget> pages = <Widget>[
    Center(
      child: Text('Home'),
    ),
    Center(
      child: Text('Categories'),
    ),
    Center(
      child: Text('Refer & Earn'),
    ),
    Center(
      child: Text('Cart'),
    ),
    Login(),
  ];
  void onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
      // body: pages.elementAt(selectedIndex),
      tabBar: CupertinoTabBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home_outlined,
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.grid_view),
            label: 'Categories',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.connect_without_contact),
            label: 'Refer & Earn',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart_outlined),
            label: 'Cart',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person_outline),
            label: 'Profile',
          ),
        ],
        currentIndex: selectedIndex,
        activeColor: Colors.amber,
        inactiveColor: Colors.black54,
        onTap: onItemTapped,
      ),
      tabBuilder: (BuildContext context, int index) {
        switch (index) {
          case 0:
            return CupertinoTabView(
              builder: (context) {
                return CupertinoPageScaffold(
                  child: Home(),
                );
              },
            );

          case 1:
            return CupertinoTabView(
              builder: (context) {
                return CupertinoPageScaffold(
                  child: Scaffold(
                    body: Center(
                      child: Text('Categories'),
                    ),
                  ),
                );
              },
            );
          case 2:
            return CupertinoTabView(
              builder: (context) {
                return CupertinoPageScaffold(
                  child: Scaffold(
                    body: Center(
                      child: Text('Refer and Earn'),
                    ),
                  ),
                );
              },
            );
          case 3:
            return CupertinoTabView(
              builder: (context) {
                return CupertinoPageScaffold(
                  child: Scaffold(
                    body: Center(
                      child: Text('Cart'),
                    ),
                  ),
                );
              },
            );
          case 4:
            return CupertinoTabView(
              builder: (context) {
                return CupertinoPageScaffold(
                  child: Login(),
                );
              },
            );

          default:
            return CupertinoTabView(
              builder: (context) {
                return CupertinoPageScaffold(
                  child: Text('Home'),
                );
              },
            );
        }
      },
    );
  }
}
